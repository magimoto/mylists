package pl.sda.zadania.jedenaste;


        import java.util.ArrayList;
        import java.util.Arrays;
        import java.util.List;
        import java.util.Random;

public class Listy {
    public static void main(String[] args) {
        Random los = new Random();

        List<Integer> lista1 = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++){
            lista1.add(los.nextInt(10));
        }
        List<Integer> lista2 = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++){
            lista2.add(los.nextInt(10));
        }
        System.out.println(lista1);
        System.out.println(lista2);
//        int [] tab = new int[10];
//        for (int i = 0; i < tab.length ; i++) {
//            tab[i] = los.nextInt(10);
//        }
//        System.out.println(Arrays.toString(tab));
//        System.out.println(porownanie(lista,tab));
//        List<Integer>lista2 = copyList(lista);
//        System.out.println(lista2);
//        System.out.println(przypisanie(tab));
        List<Integer> mergedList = new ArrayList<>();
        mergedList = merge(lista1,lista2);
        System.out.println(mergedList);
    }
    public static int porownanie(List<Integer> list, int[] tab){
        int tmp = 0;
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i) == tab[i]){
                tmp++;
            }
        }
        return tmp;
    }
    public static List<Integer> copyList(List<Integer> lista){
        return new ArrayList<>(lista);
    }
    public static List<Integer> przypisanie(int[]tab){
        List<Integer> lista = new ArrayList<Integer>();
        for (int i = 0; i < tab.length; i++) {
            lista.add(tab[i]);
        }
        return lista;
    }
    public static <T> List<T> merge(List<T>... listy) {
        List<T> newList = new ArrayList<>();
        for (int i = 0; i < listy.length; i++) {
            newList.addAll(listy[i]);
        }
        return newList;
    }
}