package pl.sda.zadania.pietnaste;

public class Phone {
    boolean callOn;
    long phoneNumber;
    String displayContent=toString();
    long numberCalled;

    public boolean isCallOn() {
        return callOn;
    }

    public void setCallOn(boolean callOn) {
        this.callOn = callOn;
    }

    public long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public void call (long anyNumber){
        if (callOn==false){
            callOn=true;
            numberCalled = anyNumber;
            System.out.println(toString());
        }else System.out.println("Zajęte");
    }
    public void endCall (){
        if (callOn){
            callOn=false;
        }else System.out.println("Brak aktywnych połączeń");
    }

    @Override
    public String toString() {
        return "Rozmowa z numerem: " + numberCalled;
    }
}
