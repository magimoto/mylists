package pl.sda.zadania.czternaste;

public class Main {


    public static void main(String[] args) {
        Parcel p1 = new Parcel("bob",true);
        System.out.println(p1);
        p1.send();
        System.out.println(p1);
        p1.sendDirect();
        System.out.println(p1);
        p1.setSender("Max");
        p1.sendDirect();
        System.out.println(p1);


    }

}
