package pl.sda.zadania.czternaste;

public class Parcel {
    String sender;
    String receiver;
    boolean sent = false;
    boolean hasContent = false;


    public Parcel(String reveiver, boolean hasContent) {
        this.receiver = reveiver;
        this.hasContent = hasContent;

    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean isHasContent() {
        return hasContent;
    }

    public void setHasContent(boolean hasContent) {
        this.hasContent = hasContent;
    }

    public void send() {

        if (receiver != null && hasContent) {
            System.out.println("Paczka jest OK!");
            sent = true;
        } else return;

    }

    public void sendDirect() {
        if (sender != null && receiver != null && hasContent) {
            System.out.println("Paczka OK!");
            System.out.println("Może być wysłana poleconym");
            sent = true;
        } else System.out.println("Dane niekompletne");
    }

    @Override
    public String toString() {
        return "Parcel{" +
                "sender='" + sender + '\'' +
                ", receiver='" + receiver + '\'' +
                ", sent=" + sent +
                ", hasContent=" + hasContent +
                '}';
    }
}
