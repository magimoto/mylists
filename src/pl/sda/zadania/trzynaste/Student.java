package pl.sda.zadania.trzynaste;

import javax.xml.bind.SchemaOutputResolver;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Student {
    private long indexNumber;
    String name;
    String Surname;
    List<Integer> marksList;
    Marks marks= new Marks();
    public Student(long indexNumber, String name, String surname, List<Integer> marksList) {
        this.indexNumber = indexNumber;
        this.name = name;
        Surname = surname;
        this.marksList = marksList;
    }

    @Override
    public String toString() {
        return "Student{" +
                "indexNumber=" + indexNumber +
                ", name='" + name + '\'' +
                ", Surname='" + Surname + '\'' +
                ", marksList=" + marksList +
                ", marks=" + marks +
                '}';
    }

    public void setIndexNumber(long indexNumber) {
        this.indexNumber = indexNumber;
    }

    public long getIndexNumber() {
        return indexNumber;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return Surname;
    }

    public List<Integer> getMarksList() {
        return marksList;
    }

    public Marks getMarks() {
        return marks;
    }

    public void setName(String name) {
        System.out.println("wprowadz nowe imie");
        Scanner scanner = new Scanner(System.in);
        this.name = scanner.nextLine();
    }

    public Student() {

    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public void setMarksList(List<Integer> marksList) {
        this.marksList = marksList;
    }

    public void setMarks(Marks marks) {
        this.marks = marks;
    }
}
