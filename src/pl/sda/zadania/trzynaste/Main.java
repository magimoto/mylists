package pl.sda.zadania.trzynaste;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Marks marks = new Marks();
        List<Integer> student1Marks = marks.marksGenerator();
        Student student1 = new Student(100400L,"Max","Brown", student1Marks);
        System.out.println(student1);
        student1.marks.getAverage(student1Marks);
        System.out.println();
        student1.marks.didYouPass(student1Marks);
        student1.setName("Maxim");
        System.out.println(student1);
        System.out.println(student1.getIndexNumber());
    }
}
