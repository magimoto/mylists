package pl.sda.zadania.trzynaste;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Marks {
    public List<Integer> marksGenerator() {
        Random random = new Random();
        int tmpMark = random.nextInt(5) + 1;
        int tmpNumberOfMarks = random.nextInt(7);
        List<Integer> tmpList = new ArrayList<>();
        for (int i = 0; i < tmpNumberOfMarks; i++) {
            tmpMark = random.nextInt(5) + 1;
            tmpList.add(tmpMark);

        }
        return tmpList;
    }

    public double averageOfMarks(List<Integer> list) {
        double sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i);
        }
        sum = sum / list.size();
        sum = Double.valueOf(sum);
        return sum;
    }

    public void getAverage(List<Integer> list) {

        System.out.printf("Srednia ocen to %.2f", averageOfMarks(list));
    }

    public void didYouPass(List<Integer> list) {
        for (Integer element : list) {
            if (element == 1 || element == 2) {
                System.out.println("Nie zdałeś!");
                return;
            }
        }
        System.out.println("Zdałeś!");
    }
}
